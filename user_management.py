from app import app, conn
import flask
from flask import request, render_template, abort
from flask_login import login_required, logout_user, current_user
from forms import LoginForm, SignupForm
from apsw import Error
from base64 import b64decode
from werkzeug.datastructures import WWWAuthenticate
from http import HTTPStatus
from bcrypt import gensalt, hashpw, checkpw


# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"


# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    stmt = f"SELECT username FROM users;"
    try:
        c = conn.cursor().execute(stmt).fetchall()
    except Error as e:
        return ()
    
    c = [i[0] for i in c]

    if user_id not in c:
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        u = users.get(uid)
        if u: # and check_password(u.password, passwd):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        for uid in users:
            if users[uid].get('token') == auth_params:
                return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('index_html'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    # check if user is already logged on
    #if current_user.is_authenticated: 
    #    return flask.redirect(flask.url_for('index_html'))


    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        
        user = user_loader(username)
        if (user == None):
                return render_template('./login.html', form=form)

        passHash = conn.execute(f"SELECT password FROM users WHERE username = ? ;", [username]).fetchone()[0]

        if (checkpw(password.encode("utf_8"),passHash.encode("utf_8")) ): # and check_password(u.password, password):
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
    return render_template('./login.html', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        passord_confirm = form.password_confirm.data
    
        if user_loader(username) != None:
            print(f"username {username} already in use")
            return render_template('./createUser.html', form=form)

        if not validPass(password) or password != passord_confirm:
            print("password not valid or passwords not the same")
            return render_template('./createUser.html', form=form)
        if not safeUsername(username):
            print("username is not valid, may contain scary chars")
            return render_template('./createUser.html', form=form)

        next = flask.request.args.get('next')

        # is_safe_url should check if the url is safe for redirects.
        # See http://flask.pocoo.org/snippets/62/ for an example.
        if False and not is_safe_url(next):
            return flask.abort(400)

        passHash = (hashpw(password.encode("utf-8"), gensalt())).decode("utf-8")
        conn.execute((f'INSERT INTO users (username, password) values ( ?, ?)'), (username, passHash))

        return flask.redirect(next or flask.url_for('index_html'))

    return render_template('./createUser.html', form=form)

@app.get('/getCurrentUsername')
def getCurrentUsername():
    return current_user.id

def validPass(password):
    if len(password) < 8:
        return False
    # contains at least one uppercase and one lowercase letter
    elif not any(char.isupper() for char in password) or not any(char.islower() for char in password):
        return False
    # contains at least one digit
    elif not any(char.isdigit() for char in password):
        return False
    else:
        return True
    
def safeUsername(username):
    if '&' in username or '<' in username or '>' in username or '"' in username or "'" in username:
        return False
    return True