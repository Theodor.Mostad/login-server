from datetime import datetime
from flask import Flask, send_from_directory, make_response, render_template
from flask_login import login_required
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from flask_wtf.csrf import CSRFProtect

tls = local()
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
from secrets import token_hex
app.secret_key = token_hex()

csrf = CSRFProtect(app)

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return render_template('index.html')


@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp


try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()

    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')

    # session data
    c.execute('''CREATE TABLE IF NOT EXISTS sessionData (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL,
        password TEXT NOT NULL);''')

    # auth data
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        username TEXT PRIMARY KEY,
        password TEXT NOT NULL);''')

    # application data
    c.execute('''CREATE TABLE IF NOT EXISTS chats (
        id integer PRIMARY KEY,
        messages TEXT NOT NULL,
        profile1 TEXT NOT NULL,
        profile2 TEXT NOT NULL);''')

    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY,
        sender TEXT NOT NULL,
        recipient TEXT NOT NULL,
        message TEST NOT NULL,
        timestamp DATETIME NOT NULL,
        replyID integer);''')

except Error as e:
    print(e)
    sys.exit(1)

import messaging