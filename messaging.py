from app import pygmentize, app, conn
from datetime import datetime
from flask import request
from flask_login import current_user, login_required
from json import dumps
from apsw import Error
from markupsafe import escape
from user_management import user_loader


@app.get('/search')
@login_required
def search():
    query = request.args.get('q') or request.form.get('q')
    try:
        if (query == "*"):
            user = current_user.id
            stmt = f"SELECT * FROM messages WHERE sender = ? OR recipient = ?"
            c = conn.execute(stmt, (user, user))
        else:
            user = current_user.id
            stmt = f"SELECT * FROM messages WHERE id = ? AND (sender = ? OR recipient = ?)"
            c = conn.execute(stmt, (query, user, user))

        result = f"Query: {pygmentize(stmt)}\n"

        rows = c.fetchall()
        print(rows)
        if not rows:
            result = "Found no messages for you. See all your messages over there →"
        else:
            result = result + 'Result:\n'
            for row in rows:
                result = (f'{result}    {dumps(escape(str(row)))}\n')
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
@login_required
def send():
    print(current_user.id)
    try:
        sender = current_user.id
        recipient = request.args.get('recipient') or request.form.get('recipient')
        replyID = request.args.get('replyID') or request.form.get('replyID')
        message = request.args.get('message') or request.args.get('message')
        timestamp = datetime.now()
        
        if not sender or not message or not recipient:
            return f'ERROR: invalid message or recipient'
        if user_loader(recipient) == None:
            return f'ERROR: invalid sender or message or recipient'

        stmt = f"INSERT INTO messages (sender, recipient, message, timestamp, replyID) values ( ?, ?, ?, ?, ?);"

        
        result = f"Query: {pygmentize(stmt)}\n"
        conn.execute(stmt, (sender, recipient, message, f'{timestamp}', replyID))
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

@app.route('/chats', methods=['GET'])
@login_required
def chats():
    stmt = f"SELECT * FROM messages WHERE sender= ? OR recipient= ? ;"
    try:
        c = conn.execute(stmt, (current_user.id, current_user.id))
        rows = c.fetchall()
        result = ""
        for row in rows:
            result += f'\n    {dumps(escape(str(row)))}\n'
        c.close()
        return result
    except Error as e:
        return (f'ERROR: {e}', 500)
