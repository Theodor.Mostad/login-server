--- I was very short on time, so I must apologize for this very poorly written documentation. ---

Make a note of what's wrong with the structure (and anything else you see that may be problematic). Could any of these things have a security impact?
	• The whole application is put into one file.
		○ Any part of the application could have unnecessary access to other parts of the application. The scopes are easier to manage with better structure.
	• Not flask standard
	• Unused files and functions (security vulnerability)
		○ This could be used to open backdoors and such
	• Bad readability makes a lot of problems
		○ Further development is harder
		○ Locating bugs, vulnerabilities etc.


	• The login management/system is bad
		○ Doesn't even check the password, only checks if the user exists
	• The so called messaging system is extremely primitive.
		○ One can search through every message in the database
		○ One can send messages with the sender being anyone they want.
		○ It is vulnerable to SQL injections

A brief overview of your design considerations from Part A
	- I took the decision to wait with the refactoring till I had a better overview of what the application consisted off.
	- I later found examples of a simple flask structure (link). That consisted off a package containing the python/application files. A /static for styles, images and such. /templates for the .html documents.
	- The frontend design.
		○ I decided to split the main home page in two.
		○ The initial idea was to have all the user's "chats", grouping all the messages by sender and recipient, but I did not have the time to that.
		○ So the simpler version with the status, off send messages and such, on the left and all the logged on user's messages on the right (blue area).
		○ Other then that it is possible to retrieve messages. One can send a message to any user. You can add the ID of the message you are replying to.
	- User management
		○ There is a simple login screen. Self explanatory a would think. One can create a user by filling a sign up form, similar to the login form. 
		
instructions on how to test/demo it:
		○ There is a "create user" button on the login screen where you get sent to the sign up page. When you sign up, and all the all the inputs are valid (see creating a user in technical details), press enter or the create user button. You are then sent back to the login page where you can login with your new user.
		○ Now you can retrieve messages by message ID and clicking the "Retrieve!" button, but you don't have any messages yet. Until you have a ton off messages you can easily see all your messages on the right.
		○ You can send messages to any user by typing the username in the "To" input, but the user have to exist. Write your message in the "message" input. Add the message ID of the message you are replying to in the "Reply" input. Then send it by clicking the "send" button.
		○ Log out by clicking the "log out" button. This send you back the login page.

technical details on the implementation:
		○ I am going to skip the parts that was already implemented.
	- Creating a user is handled by the signup() function. The inputs username, password and confirm_password is loaded. The username is checked against the database, if the username already exists nothing happens you have to try again. We then check that the passwords are alike and that the they fulfil the requirements. Basic requirements are used, at least 8 characters long, contains at least one digit, uppercase and lowercase letters. The username can not contain the basic dangerous web characters <, >, &, " and '. If all is good, the password is hashed together with a salt and stored together with the username in the users database. Bycrypt's hashpw() and gensalt() functions are used.
	- Login is handled by the login() function (probably mainly by Flask but). After the username and password is loaded in from the LoginForm(), it checks if the user exists in the database, if not nothing happens. If user is found the inputted password is checked against the hashed password stored with the user in the database. This is done by bcrypt's imported function checkpw(p1,p2). If this is successful you are redirected to the index page.
	- Logout is handled in the logout() function, by Flask's logout_user() function. If successful you are redirected to the login page.
	- Database handling is done with SQLite. To prevent SQL injection attacks I used prepared statements with parameterized queries (sql injection prevention).
	- Messaging system is protected by csrf's token system to prevent cross-site request forgery. 
	- HTML XSS/html injection is prevented by applying escape to everything that is retrieved from the database and put into the html. Further protection is applied with the .textContent
	- Session protection, the session data is protected by a random secret key provided by secret's function token_hex().

Questions
	• Threat model – 
		○ who might attack the application? 
			§ Adversaries who might want user data, messages, or any kind of data available.
		○ What damage could be done (in terms of confidentiality, integrity, availability)? 
			§ Confidentiality
				□ Steal data
			§ Integrity
				□ Change/manipulate data
			§ Availability
				□ Encrypt data
		○ What can an attacker do?
			§ By stealing data they gather information that can be used in many different ways. Gather account info to access and misuse account. Use data to try accessing accounts on other platforms.
			§ By changing data they can 
			§ By encrypting data they can render the application useless/unavailable. They can demand money or other things to decrypt the data.
			
		○ Are there limits to what an attacker can do?
			§ Not exactly sure what is meant here. I don't think any adversary would be able to take over the world by attacking this application. UNLESS! The president off the united states decides to send the codes for the nuclear weapons in a message using my app. So no there is not any limits to what an attacker can do. UNLESS the current protection actually works.
		○ Are there limits to what we can sensibly protect against?
			§ One have to focus on protecting the most critical parts, like the user data and messages. Protecting against attacks that would not effect any critical parts of the application or would not give access to any information one should not have access to, would possibly not be the best thing to focus on protecting.

	• What are the main attack vectors for the application?
		○ DoS, overloading the app making it unavailable
		○ SQL injections
		○ JS injections
	• What should we do (or what have you done) to protect against attacks?
		○ See Technical details for what I have done.
		○ I should probably have done a lot more testing.
		○ All critical application data like the tokens and secret keys and such should probably have been secured in a better way, than just being variables scattered around.
	• What is the access control model?
		○ Mandatory Access Control with Discretionary Access Control. One have to be logged to see any thing (MAC). There are messages that only the sender and recipient can see (DAC).
	• How can you know that you security is good enough? (traceability)
		○ Assuming that my application is fault proof when it comes to that you cant change the sender of a message, it is always the logged on user, you can trace who sent every message :) 
	

Oblig3 - REVIEW
Do you understand the overall design, based on the documentation and available source code?
The overall design and structure are not perfect but pretty good. First off, the documentation is well and clearly written. How to get the app up and running and what you can do is nicely explained. He clearly states that he might have distributed his time a bit badly. That you can clearly see in the app, where the login and signup is beautiful but when you get in, nothing works except logging back out. 
The source code looks pretty good, with a lot off (maybe too many) comments explaining how function works. Still, he has a lot of unused imports and some unused code.

Do you see any potential improvements that could be made in design / coding style / best practices? I.e., to make the source code clearer and make it easier to spot bugs.
Code:
There are some wired design choices that are made. Like the database declaration and initialisation is located in the file password_handling.py. So are most functions associated with messaging. Then I would think the login handling would be located here, as they handle passwords, but they are not... 
Spotting of bugs and clearness is basically what already wrote a bit about in the previous question about the comments and stuff.
GUI
Looks pretty good!

Based on the threat model described by the author(s) (from part 2B), do you see any vulnerabilities in the application?
Yes, all of them. Or none of them. Since no function is even working in the backend. But if we hypothetically said that things are working, he has a bit protection. Some Functions like send are protected against SQL injections by prepared statements, but other like search or not. You are also able to see every message from every user, the author is aware off this. He has tried protecting against cross site request forgery with the use of tokens in some of the frontend functions, doesn't work off course.
He has not mentioned cryptographic failures and vulnerable and outdated components. All others are mentioned in one way or another. His threat model is fairly comprehensive.
what sort of damage could you do by tricking a logged-in user into click on a link?
Again, if we say that everything works. He has tried implementing tokens for sending messages and making “conversations”, which I believe was an attempt to mitigate CSRF.

